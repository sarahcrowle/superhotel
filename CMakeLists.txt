cmake_minimum_required(VERSION 3.12)

project(shemu)

find_package(SDL2 REQUIRED)
find_package(OpenGL REQUIRED)

set(SOURCES
	src/main.cpp
	src/instructions.cpp
	src/instructions.h
	src/endianness_util.h
	src/sh_emu.h)

set (IMGUI
	 imgui/imgui.cpp
	 imgui/imgui_draw.cpp
	 imgui/imgui_impl_opengl3.cpp
	 imgui/imgui_impl_sdl2.cpp
	 imgui/imgui_tables.cpp
	 imgui/imgui_widgets.cpp)

add_executable(shemu ${SOURCES} ${IMGUI})
target_include_directories(shemu PRIVATE src/)
target_include_directories(shemu PRIVATE imgui/)
target_include_directories(shemu PRIVATE ${SDL2_INCLUDE_DIRS})
target_link_libraries(shemu PRIVATE ${SDL2_LIBRARIES} ${OPENGL_LIBRARIES})

set_property(TARGET shemu PROPERTY CXX_STANDARD 20)
