#!/bin/sh

sh-elf-as --renesas --big -o test.o test.s
sh-elf-ld --oformat binary -o test.bin test.o
truncate -s 16M test.bin