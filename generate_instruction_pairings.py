instructions = []
with open("instructions.txt", "r") as instructions_list:
    instructions = [(i.split(" ")[-1], " ".join(i.split(" ")[:-1])) for i in instructions_list.readlines()]

#print(instructions)

for instruction in instructions:
    encoding, desc = instruction
    encoding = encoding.replace("\n", "")

    print(f"{{ \"{encoding}\", SH_EMU_UNIMPLEMENTED }}, // {desc}")
