# a lil SuperH

A SuperH emulator! Operates on raw binary machine code only. It's incomplete!

Build with `./build.sh`.

See `./buildtest.sh` for an example of how to build an assembly file.

It should be fairly simple to build a basic SuperH toolchain, and the [POBARISNA toolchain scripts](https://gitlab.com/sarahcrowle/pobarisna-toolchain) work pretty well for that purpose. If you're on Linux you might have a bare sh-elf-gcc somewhere in your preferred distro's repos. 

If you're lazy, there's a sample test.bin in this repo. What does it do? Good question! It depends what I was last implementing, and I commit new versions of it completely at random.

Run with `./shemu <binary>`

## Helpful docs

- [Hitachi SuperH RISC Engine SH-1/SH-2 Programmer's Guide](https://antime.kapsi.fi/sega/files/h12p0.pdf)
- [Renesas SH Instruction Set Summary](http://shared-ptr.com/sh_insns.html) ([Repo](https://github.com/shared-ptr/sh_insns))

## Progress:

19/142 instruction encodings implemented (13.3%)

## License

MIT. See LICENSE.txt
