#pragma once

#include <map>
#include <iostream>
#include <functional>
#include <endianness_util.h>
#include <sh_emu.h>

#define SH_EMU_UNIMPLEMENTED [](sh_emu_state *state){ std::cerr << "unimplemented instruction @ pc 0x" << std::hex << state->cpu.system_registers.pc << std::dec; }

extern std::map<std::string, std::function<void(sh_emu_state*)>> instructions_map;

void dispatch_instruction(sh_emu_state *state, be_uint16_t instruction);