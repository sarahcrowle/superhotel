#include <iostream>
#include <fstream>
#include <assert.h>

#include <imgui.h>
#include <imgui_impl_sdl2.h>
#include <imgui_impl_opengl3.h>

#include <SDL.h>
#include <SDL_opengl.h>

#include <endianness_util.h>
#include <sh_emu.h>
#include <instructions.h>

int main(int argc, char **argv) {
    // Setup SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }
    
    // pick GL ver
    #if defined(__APPLE__)
        // GL 3.2 Core + GLSL 150
        const char* glsl_version = "#version 150";
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    #else
        // GL 3.0 + GLSL 130
        const char* glsl_version = "#version 130";
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    #endif
    
    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Window* window = SDL_CreateWindow("SuperHotel", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version);
    
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    
    // FIXME: binary size limit?
    char *binary_data = new char[1024 * 64];
    std::ifstream fin(argv[1], std::ios::in | std::ios::binary);
    fin.read(binary_data, 1024 * 64);

    sh_emu_state *state = new sh_emu_state;
    state->instructions = reinterpret_cast<be_uint16_t *>(binary_data);
    
    bool done = false;
    while (!done) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();
        
        ImGui::Begin("Control");
        if (ImGui::Button("Step")) {
            // if we have a delay slot, then dispatch the instruction at the instruction stored
            //  in the slot. if there's no valid instruction there, then dispatch the 
            //  instruction normally. if there's nothing in the delay slot, dispatch the 
            //  instruction at the PC.
            if (state->delay_slot) {
                if (state->instructions[state->delay_slot]) {
                    dispatch_instruction(state, state->instructions[state->delay_slot]);
                    state->delay_slot = 0;
                } else {
                    state->delay_slot = 0;
                    goto normal_dispatch;
                }
            } else {
        normal_dispatch:
                dispatch_instruction(state, state->instructions[state->cpu.system_registers.pc]);
                state->cpu.system_registers.pc = static_cast<uint32_t>(state->cpu.system_registers.pc) + 1;
            }
        }
        ImGui::End();
        
        // get the native versions of the registers FIXME: this is a bit weird
        uint32_t r[16];
        for (int i = 0; i < 16; i++) {
            r[i] = state->cpu.general_registers.r[i];
        }
        
        uint32_t mach = state->cpu.system_registers.mach;
        uint32_t macl = state->cpu.system_registers.macl;
        uint32_t pc = state->cpu.system_registers.pc;
        uint32_t pr = state->cpu.system_registers.pr;
        
        uint32_t sr = state->cpu.control_registers.sr;
        uint32_t gbr = state->cpu.control_registers.gbr;
        uint32_t vbr = state->cpu.control_registers.vbr;
        
        ImGui::Begin("General Registers");
            ImGui::Text("r0: 0x%x  // r1: 0x%x  // r2: 0x%x  // r3: 0x%x",
                        r[0], r[1],
                        r[2], r[3]);
            ImGui::Text("r4: 0x%x  // r5: 0x%x  // r6: 0x%x  // r7: 0x%x",
                        r[4], r[5],
                        r[6], r[7]);
            ImGui::Text("r8: 0x%x  // r9: 0x%x  // r10: 0x%x // r11: 0x%x",
                        r[8],  r[9],
                        r[10], r[11]);
            ImGui::Text("r12: 0x%x // r13: 0x%x // r14: 0x%x // r15: 0x%x",
                        r[12], r[13],
                        r[14], r[15]);
        ImGui::End();
        
        ImGui::Begin("System Registers");
            ImGui::Text("mach: 0x%x // macl: 0x%x", mach, macl);
            ImGui::Text("pc: 0x%x // pr: 0x%x", pc, pr);
        ImGui::End();
        
        ImGui::Begin("Control Registers");
            ImGui::Text("sr: 0b%s", std::bitset<16>(sr).to_string().c_str());
            ImGui::Text("gbr: 0x%x // vbr: 0x%x", gbr, vbr);
        ImGui::End();
        
        ImGui::Begin("Log");
            ImGui::Text("%s", state->log.str().c_str());
            if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
                ImGui::SetScrollHereY(1.0f);
        ImGui::End();
        
        ImGui::Render();
        glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        SDL_GL_SwapWindow(window);
    }
    
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
