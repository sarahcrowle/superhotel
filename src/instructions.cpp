#include <bitset>
#include <string>
#include <cctype>

#include <instructions.h>
#include <endianness_util.h>

uint16_t extract_bits(uint16_t value, int begin, int end) {
    uint16_t mask = (1 << (end - begin)) - 1;
    return (value >> begin) & mask;
}

std::string as_binary_string(uint16_t value) {
    std::string binary = std::bitset<16>(value).to_string();
    return binary;
}

// a map of instruction encodings to instruction implementations. see dispatch_instruction()
std::map<std::string, std::function<void(sh_emu_state*)>> instructions_map = {
    { "0000000000001000", [](sh_emu_state *state) { // CLRT
        // clear the T bit in SR
        uint32_t SR_LE = state->cpu.control_registers.sr;
        SR_LE &= ~(1 << 0);

        state->cpu.control_registers.sr = SR_LE;
    } },
    { "0000000000101000", [](sh_emu_state *state) { // CLRMAC
        // clear both MACx registers
        state->cpu.system_registers.macl = 0;
        state->cpu.system_registers.mach = 0;
    } },
    { "0000000000011001", [](sh_emu_state *state) { // DIV0U
        // clear T, M, Q bits in SR
        uint32_t SR_LE = state->cpu.control_registers.sr;
        SR_LE &= ~(1 << 0);
        SR_LE &= ~(1 << 8);
        SR_LE &= ~(1 << 9);

        state->cpu.control_registers.sr = SR_LE;
    } },
    { "0000000000001001", [](sh_emu_state *state) {} }, // NOP
    { "0000000000101011", SH_EMU_UNIMPLEMENTED }, // RTE
    { "0000000000001011", SH_EMU_UNIMPLEMENTED }, // RTS
    { "0000000000011000", [](sh_emu_state *state) { // SETT
        // set the T bit in SR
        uint32_t SR_LE = state->cpu.control_registers.sr;
        SR_LE |= 1 << 0;

        state->cpu.control_registers.sr = SR_LE;
    } },
    { "0000000000011011", [](sh_emu_state *state) { // SLEEP
        state->log << "sleeping\n";
        while (true) {}
    } },
    { "0100nnnn00010101", SH_EMU_UNIMPLEMENTED }, // CMP/PL Rn
    { "0100nnnn00010001", SH_EMU_UNIMPLEMENTED }, // CMP/PZ Rn
    { "0100nnnn00010000", SH_EMU_UNIMPLEMENTED }, // DT Rn*
    { "0000nnnn00101001", SH_EMU_UNIMPLEMENTED }, // MOVT Rn
    { "0100nnnn00000100", SH_EMU_UNIMPLEMENTED }, // ROTL Rn
    { "0100nnnn00000101", SH_EMU_UNIMPLEMENTED }, // ROTR Rn
    { "0100nnnn00100100", SH_EMU_UNIMPLEMENTED }, // ROTCL Rn
    { "0100nnnn00100101", SH_EMU_UNIMPLEMENTED }, // ROTCR Rn
    { "0100nnnn00100000", SH_EMU_UNIMPLEMENTED }, // SHAL Rn
    { "0100nnnn00100001", SH_EMU_UNIMPLEMENTED }, // SHAR Rn
    { "0100nnnn00000000", SH_EMU_UNIMPLEMENTED }, // SHLL Rn
    { "0100nnnn00000001", SH_EMU_UNIMPLEMENTED }, // SHLR Rn
    { "0100nnnn00001000", SH_EMU_UNIMPLEMENTED }, // SHLL2 Rn
    { "0100nnnn00001001", SH_EMU_UNIMPLEMENTED }, // SHLR2 Rn
    { "0100nnnn00011000", SH_EMU_UNIMPLEMENTED }, // SHLL8 Rn
    { "0100nnnn00011001", SH_EMU_UNIMPLEMENTED }, // SHLR8 Rn
    { "0100nnnn00101000", SH_EMU_UNIMPLEMENTED }, // SHLL16 Rn
    { "0100nnnn00101001", SH_EMU_UNIMPLEMENTED }, // SHLR16 Rn
    { "0011nnnnmmmm1100", [](sh_emu_state *state) { // ADD Rm,Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);
        
        state->log << "ADD Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
        
        state->cpu.general_registers.r[n] = state->cpu.general_registers.r[n] + state->cpu.general_registers.r[m];
        
        state->log << "AFTER! ADD Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
    } },
    { "0011nnnnmmmm1110", SH_EMU_UNIMPLEMENTED }, // ADDC Rm,Rn
    { "0011nnnnmmmm1111", SH_EMU_UNIMPLEMENTED }, // ADDV Rm,Rn
    { "0010nnnnmmmm1001", [](sh_emu_state *state) { // AND Rm,Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "AND Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = state->cpu.general_registers.r[m] &
                                             state->cpu.general_registers.r[n];

        state->log << "AFTER! AND Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";
    } },
    { "0011nnnnmmmm0000", SH_EMU_UNIMPLEMENTED }, // CMP/EQ Rm,Rn
    { "0011nnnnmmmm0010", SH_EMU_UNIMPLEMENTED }, // CMP/HS Rm,Rn
    { "0011nnnnmmmm0011", SH_EMU_UNIMPLEMENTED }, // CMP/GE Rm,Rn
    { "0011nnnnmmmm0110", SH_EMU_UNIMPLEMENTED }, // CMP/HI Rm,Rn
    { "0011nnnnmmmm0111", SH_EMU_UNIMPLEMENTED }, // CMP/GT Rm,Rn
    { "0010nnnnmmmm1100", SH_EMU_UNIMPLEMENTED }, // CMP/STR Rm,Rn
    { "0011nnnnmmmm0100", SH_EMU_UNIMPLEMENTED }, // DIV1 Rm,Rn
    { "0010nnnnmmmm0111", SH_EMU_UNIMPLEMENTED }, // DIV0S Rm,Rn
    { "0011nnnnmmmm1101", SH_EMU_UNIMPLEMENTED }, // DMULS.L Rm,Rn
    { "0011nnnnmmmm0101", SH_EMU_UNIMPLEMENTED }, // DMULU.L Rm,Rn
    { "0110nnnnmmmm1110", SH_EMU_UNIMPLEMENTED }, // EXTS.B Rm,Rn
    { "0110nnnnmmmm1111", SH_EMU_UNIMPLEMENTED }, // EXTS.W Rm,Rn
    { "0110nnnnmmmm1100", SH_EMU_UNIMPLEMENTED }, // EXTU.B Rm,Rn
    { "0110nnnnmmmm1101", SH_EMU_UNIMPLEMENTED }, // EXTU.W Rm,Rn
    { "0110nnnnmmmm0011", SH_EMU_UNIMPLEMENTED }, // MOV Rm,Rn
    { "0000nnnnmmmm0111", [](sh_emu_state *state) {  // MUL.L Rm,Rn*2
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "MUL.L Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";

        state->cpu.system_registers.macl = state->cpu.general_registers.r[m] * 
                                            state->cpu.general_registers.r[n];
        
        state->log << "AFTER! MUL.L Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
    } },
    { "0010nnnnmmmm1111", SH_EMU_UNIMPLEMENTED }, // MULS.W Rm,Rn
    { "0010nnnnmmmm1110", SH_EMU_UNIMPLEMENTED }, // MULU.W Rm,Rn
    { "0110nnnnmmmm1011", [](sh_emu_state *state) { // NEG Rm,Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "NEG Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = -state->cpu.general_registers.r[m];
        
        state->log << "AFTER! NEG Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
    } },
    { "0110nnnnmmmm1010", SH_EMU_UNIMPLEMENTED }, // NEGC Rm,Rn
    { "0110nnnnmmmm0111", [](sh_emu_state *state) { // NOT Rm,Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "NOT Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = ~state->cpu.general_registers.r[m];

        state->log << "AFTER! NOT Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";
    } },
    { "0010nnnnmmmm1011", [](sh_emu_state *state) { // OR Rm, Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "OR Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = state->cpu.general_registers.r[m] |
                                             state->cpu.general_registers.r[n];

        state->log << "AFTER! OR Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";
    } },
    { "0011nnnnmmmm1000", [](sh_emu_state *state) { // SUB Rm,Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "SUB Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = state->cpu.general_registers.r[n] -
                                             state->cpu.general_registers.r[m];
        
        state->log << "AFTER! SUB Rm,Rn // m: " << m
                  << "(" << state->cpu.general_registers.r[m] << ")"
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
    } },
    { "0011nnnnmmmm1010", SH_EMU_UNIMPLEMENTED }, // SUBC Rm,Rn
    { "0011nnnnmmmm1011", SH_EMU_UNIMPLEMENTED }, // SUBV Rm,Rn
    { "0110nnnnmmmm1000", SH_EMU_UNIMPLEMENTED }, // SWAP.B Rm,Rn
    { "0110nnnnmmmm1001", SH_EMU_UNIMPLEMENTED }, // SWAP.W Rm,Rn
    { "0010nnnnmmmm1000", SH_EMU_UNIMPLEMENTED }, // TST Rm,Rn
    { "0010nnnnmmmm1010", [](sh_emu_state *state) { // XOR Rm, Rn
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 4, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "XOR Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";

        state->cpu.general_registers.r[n] = state->cpu.general_registers.r[m] ^
                                             state->cpu.general_registers.r[n];

        state->log << "AFTER! XOR Rm,Rn // m: " << m
                  << "(" << as_binary_string(state->cpu.general_registers.r[m]) << ")"
                  << " // n: " << n
                  << "(" << as_binary_string(state->cpu.general_registers.r[n]) << ")"
                  << "\n";
    } },
    { "0010nnnnmmmm1101", SH_EMU_UNIMPLEMENTED }, // XTRCT Rm,Rn
    { "0100mmmm00001110", SH_EMU_UNIMPLEMENTED }, // LDC Rm,SR
    { "0100mmmm00011110", SH_EMU_UNIMPLEMENTED }, // LDC Rm,GBR
    { "0100mmmm00101110", SH_EMU_UNIMPLEMENTED }, // LDC Rm,VBR
    { "0100mmmm00001010", SH_EMU_UNIMPLEMENTED }, // LDS Rm,MACH
    { "0100mmmm00011010", SH_EMU_UNIMPLEMENTED }, // LDS Rm,MACL
    { "0100mmmm00101010", SH_EMU_UNIMPLEMENTED }, // LDS Rm,PR
    { "0000nnnn00000010", SH_EMU_UNIMPLEMENTED }, // STC SR,Rn
    { "0000nnnn00010010", SH_EMU_UNIMPLEMENTED }, // STC GBR,Rn
    { "0000nnnn00100010", SH_EMU_UNIMPLEMENTED }, // STC VBR,Rn
    { "0000nnnn00001010", SH_EMU_UNIMPLEMENTED }, // STS MACH,Rn
    { "0000nnnn00011010", SH_EMU_UNIMPLEMENTED }, // STS MACL,Rn
    { "0000nnnn00101010", SH_EMU_UNIMPLEMENTED }, // STS PR,Rn
    { "0100mmmm00101011", [](sh_emu_state *state) { // JMP @Rm
        int instruction = state->current_instruction;
        int m = extract_bits(instruction, 8, 12);

        state->delay_slot = static_cast<uint32_t>(state->cpu.system_registers.pc) + 1;
        state->cpu.system_registers.pc = state->cpu.general_registers.r[m];

        state->log << "JMP @Rm // m: " << m << "(" 
            << state->cpu.general_registers.r[m] << ")\n";
    } },
    { "0100mmmm00001011", SH_EMU_UNIMPLEMENTED }, // JSR @Rm
    { "0100nnnn00011011", SH_EMU_UNIMPLEMENTED }, // TAS.B @Rn
    { "0010nnnnmmmm0000", SH_EMU_UNIMPLEMENTED }, // MOV.B Rm,@Rn
    { "0010nnnnmmmm0001", SH_EMU_UNIMPLEMENTED }, // MOV.W Rm,@Rn
    { "0010nnnnmmmm0010", SH_EMU_UNIMPLEMENTED }, // MOV.L Rm,@Rn
    { "0110nnnnmmmm0000", SH_EMU_UNIMPLEMENTED }, // MOV.B @Rm,Rn
    { "0110nnnnmmmm0001", SH_EMU_UNIMPLEMENTED }, // MOV.W @Rm,Rn
    { "0110nnnnmmmm0010", SH_EMU_UNIMPLEMENTED }, // MOV.L @Rm,Rn
    { "0000nnnnmmmm1111", SH_EMU_UNIMPLEMENTED }, // MAC.L @Rm+,@Rn+* 2
    { "0100nnnnmmmm1111", SH_EMU_UNIMPLEMENTED }, // MAC.W @Rm+,@Rn+
    { "0110nnnnmmmm0100", SH_EMU_UNIMPLEMENTED }, // MOV.B @Rm+,Rn
    { "0110nnnnmmmm0101", SH_EMU_UNIMPLEMENTED }, // MOV.W @Rm+,Rn
    { "0110nnnnmmmm0110", SH_EMU_UNIMPLEMENTED }, // MOV.L @Rm+,Rn
    { "0100mmmm00000111", SH_EMU_UNIMPLEMENTED }, // LDC.L @Rm+,SR
    { "0100mmmm00010111", SH_EMU_UNIMPLEMENTED }, // LDC.L @Rm+,GBR
    { "0100mmmm00100111", SH_EMU_UNIMPLEMENTED }, // LDC.L @Rm+,VBR
    { "0100mmmm00000110", SH_EMU_UNIMPLEMENTED }, // LDS.L @Rm+,MACH
    { "0100mmmm00010110", SH_EMU_UNIMPLEMENTED }, // LDS.L @Rm+,MACL
    { "0100mmmm00100110", SH_EMU_UNIMPLEMENTED }, // LDS.L @Rm+,PR
    { "0010nnnnmmmm0100", SH_EMU_UNIMPLEMENTED }, // MOV.B Rm,@–Rn
    { "0010nnnnmmmm0101", SH_EMU_UNIMPLEMENTED }, // MOV.W Rm,@–Rn
    { "0010nnnnmmmm0110", SH_EMU_UNIMPLEMENTED }, // MOV.L Rm,@–Rn
    { "0100nnnn00000011", SH_EMU_UNIMPLEMENTED }, // STC.L SR,@-Rn
    { "0100nnnn00010011", SH_EMU_UNIMPLEMENTED }, // STC.L GBR,@-Rn
    { "0100nnnn00100011", SH_EMU_UNIMPLEMENTED }, // STC.L VBR,@-Rn
    { "0100nnnn00000010", SH_EMU_UNIMPLEMENTED }, // STS.L MACH,@–Rn
    { "0100nnnn00010010", SH_EMU_UNIMPLEMENTED }, // STS.L MACL,@–Rn
    { "0100nnnn00100010", SH_EMU_UNIMPLEMENTED }, // STS.L PR,@–Rn
    { "10000000nnnndddd", SH_EMU_UNIMPLEMENTED }, // MOV.B R0,@(disp,Rn)
    { "10000001nnnndddd", SH_EMU_UNIMPLEMENTED }, // MOV.W R0,@(disp,Rn)
    { "0001nnnnmmmmdddd", SH_EMU_UNIMPLEMENTED }, // MOV.L Rm,@(disp,Rn)
    { "10000100mmmmdddd", SH_EMU_UNIMPLEMENTED }, // MOV.B @(disp,Rm),R0
    { "10000101mmmmdddd", SH_EMU_UNIMPLEMENTED }, // MOV.W @(disp,Rm),R0
    { "0101nnnnmmmmdddd", SH_EMU_UNIMPLEMENTED }, // MOV.L @(disp,Rm),Rn
    { "0000nnnnmmmm0100", SH_EMU_UNIMPLEMENTED }, // MOV.B Rm,@(R0,Rn)
    { "0000nnnnmmmm0101", SH_EMU_UNIMPLEMENTED }, // MOV.W Rm,@(R0,Rn)
    { "0000nnnnmmmm0110", SH_EMU_UNIMPLEMENTED }, // MOV.L Rm,@(R0,Rn)
    { "0000nnnnmmmm1100", SH_EMU_UNIMPLEMENTED }, // MOV.B @(R0,Rm),Rn
    { "0000nnnnmmmm1101", SH_EMU_UNIMPLEMENTED }, // MOV.W @(R0,Rm),Rn
    { "0000nnnnmmmm1110", SH_EMU_UNIMPLEMENTED }, // MOV.L @(R0,Rm),Rn
    { "11000000dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.B R0,@(disp,GBR)
    { "11000001dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.W R0,@(disp,GBR)
    { "11000010dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.L R0,@(disp,GBR)
    { "11000100dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.B @(disp,GBR),R0
    { "11000101dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.W @(disp,GBR),R0
    { "11000110dddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.L @(disp,GBR),R0
    { "11001101iiiiiiii", SH_EMU_UNIMPLEMENTED }, // AND.B #imm,@(R0,GBR)
    { "11001111iiiiiiii", SH_EMU_UNIMPLEMENTED }, // OR.B #imm,@(R0,GBR)
    { "11001100iiiiiiii", SH_EMU_UNIMPLEMENTED }, // TST.B #imm,@(R0,GBR)
    { "11001110iiiiiiii", SH_EMU_UNIMPLEMENTED }, // XOR.B #imm,@(R0,GBR)
    { "1001nnnndddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.W @(disp,PC),Rn
    { "1101nnnndddddddd", SH_EMU_UNIMPLEMENTED }, // MOV.L @(disp,PC),Rn
    { "11000111dddddddd", SH_EMU_UNIMPLEMENTED }, // MOVA @(disp,PC),R0
    { "0000mmmm00100011", SH_EMU_UNIMPLEMENTED }, // BRAF Rm*2
    { "0000mmmm00000011", SH_EMU_UNIMPLEMENTED }, // BSRF Rm*2
    { "10001011dddddddd", SH_EMU_UNIMPLEMENTED }, // BF label
    { "10001111dddddddd", SH_EMU_UNIMPLEMENTED }, // BF/S label
    { "10001001dddddddd", SH_EMU_UNIMPLEMENTED }, // BT label
    { "10001101dddddddd", SH_EMU_UNIMPLEMENTED }, // BT/S label
    { "1010dddddddddddd", SH_EMU_UNIMPLEMENTED }, // BRA label
    { "1011dddddddddddd", SH_EMU_UNIMPLEMENTED }, // BSR label
    { "0111nnnniiiiiiii", [](sh_emu_state *state) { // ADD(I) #imm,Rn
        int instruction = state->current_instruction;
        int imm = extract_bits(instruction, 0, 8);
        int n = extract_bits(instruction, 8, 12);

        state->log << "ADD(I) #imm,Rn // imm: " << imm
                  << " // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";

        if ((imm & 0x80) == 0)
            state->cpu.general_registers.r[n] = state->cpu.general_registers.r[n] + (0x000000FF & (long)imm);
        else
            state->cpu.general_registers.r[n] = state->cpu.general_registers.r[n] + (0xFF000000 | (long)imm);

        state->log << "AFTER! ADD(I) #imm,Rn // n: " << n
                  << "(" << state->cpu.general_registers.r[n] << ")"
                  << "\n";
    } },
    { "11001001iiiiiiii", [](sh_emu_state *state) { // AND #imm,R0
        int instruction = state->current_instruction;
        int imm = extract_bits(instruction, 1, 8);

        state->log << "AND #imm,R0 // r0: "
                  << "(" << as_binary_string(state->cpu.general_registers.r[0]) << ")"
                  << " // imm: " << imm
                  << "(" << as_binary_string(imm) << ")"
                  << "\n";

        state->cpu.general_registers.r[0] = state->cpu.general_registers.r[0] &
                                             (0x000000FF & (long)imm); // zero extend

        state->log << "AFTER! AND #imm,R0 // r0: "
                  << "(" << as_binary_string(state->cpu.general_registers.r[0]) << ")"
                  << " // imm: " << imm
                  << "(" << as_binary_string(imm) << ")"
                  << "\n";
    } },
    { "10001000iiiiiiii", SH_EMU_UNIMPLEMENTED }, // CMP/EQ #imm,R0
    { "1110nnnniiiiiiii", [](sh_emu_state *state) {  // MOV #imm,Rn
        int instruction = state->current_instruction;
        int i = extract_bits(instruction, 0, 8);
        int n = extract_bits(instruction, 8, 12);
        
        state->cpu.general_registers.r[n] = i;
        
        state->log << "MOV #imm,Rn // imm: " << i << " // n: " << n << "\n";
    } },
    { "11001011iiiiiiii", [](sh_emu_state *state) { // OR #imm,R0
        int instruction = state->current_instruction;
        int imm = extract_bits(instruction, 1, 8);

        state->log << "OR #imm,R0 // r0: "
                  << "(" << as_binary_string(state->cpu.general_registers.r[0]) << ")"
                  << " // imm: " << imm
                  << "(" << as_binary_string(imm) << ")"
                  << "\n";

        state->cpu.general_registers.r[0] = state->cpu.general_registers.r[0] |
                                             (0x000000FF & (long)imm); // zero extend

        state->log << "AFTER! OR #imm,R0 // r0: "
                  << "(" << as_binary_string(state->cpu.general_registers.r[0]) << ")"
                  << " // imm: " << imm
                  << "(" << as_binary_string(imm) << ")"
                  << "\n";
    } },
    { "11001000iiiiiiii", SH_EMU_UNIMPLEMENTED }, // TST #imm,R0
    { "11001010iiiiiiii", SH_EMU_UNIMPLEMENTED }, // XOR #imm,R0
    { "11000011iiiiiiii", SH_EMU_UNIMPLEMENTED }, // TRAPA #imm
};

// count the significant bits that match. 
//  encodings look like this: 0010nnnnmmmm1111 (http://shared-ptr.com/sh_insns.html)
//   - all binary digits are significant, and MUST be matched.
//   - all letters are mostly for self-documenting purposes. n = Rn, m = Rm, i = imm, etc
//  to match an encoding, we have to:
//   1. iterate through every encoding
//   2. check each character in the encoding to check if it's a digit and should be 
//      significant
//   3. check if the binary digit in our instruction at the same position as the 
//      digit we're checking matches this encoding
//   4. if the number of significant characters in the encoding matches the number
//      of matching characters we have in our instruction, we've found our instruction!
void dispatch_instruction(sh_emu_state *state, be_uint16_t instruction) {
    // first, let's get the instruction as a string.
    std::string binary = as_binary_string(instruction);

    // iterate over all of our instruction encodings
    for (const auto &[encoding, impl] : instructions_map) {
        int significant = 0;
        int matches = 0;
        int pos = 0;
        
        // check for significant and matching digits in the encoding
        for (const char &c : encoding) {
            if (isdigit(c)) {
                significant++;

                if (c == binary[pos]) {
                    matches++;
                }
            }
            pos++;
        }

        // if we matched as many characters as there are significant digits in the encoding,
        //  we have our instruction! set the current instruction in the emulator state
        //  and call the implementation of that instruction from the map above.
        if (significant == matches) {
            state->log << binary << " matched encoding: " << encoding << "\n";
            state->current_instruction = instruction;
            impl(state);
            break;
        }
    }
}

