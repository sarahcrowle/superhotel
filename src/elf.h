#pragma once

#include <map>
#include <stdint.h>
#include <string>

#include <endianness_util.h>

enum elf_classes {
    ELFCLASSNONE = 0,
    ELFCLASS32 = 1,
    ELFCLASS64 = 2
};

enum elf_endianness {
    ELFDATANONE = 0,
    ELFDATA2LSB = 1,
    ELFDATA2MSB = 2,
};

struct elf_identification {
    char magic[4];
    char elf_class;
    char endianness;
    char version;
    char padding[9];
} __attribute__ ((packed));

struct elf_header {
    elf_identification identification;

    be_uint16_t type;
    be_uint16_t machine;
    be_uint32_t version;
    be_uint32_t entry_point;
    be_uint32_t program_header_offset;
    be_uint32_t section_header_offset;
    be_uint32_t flags;
    be_uint16_t elf_header_size;
    be_uint16_t program_header_entry_size;
    be_uint16_t program_header_number_entries;
    be_uint16_t section_header_entry_size;
    be_uint16_t section_header_number_entries;
    be_uint16_t section_header_string_index;
} __attribute__ ((packed));

struct elf_program_header {
    be_uint32_t type;
    be_uint32_t offset;
    be_uint32_t virtual_address;
    be_uint32_t physical_addr;
    be_uint32_t file_size;
    be_uint32_t memory_size;
    be_uint32_t flags;
    be_uint32_t alignment;
} __attribute__ ((packed));

std::map<be_uint32_t, std::string> program_header_types_friendly = {
    { 0x0, "PT_NULL" },
    { 0x1, "PT_LOAD" },
    { 0x2, "PT_DYNAMIC" },
    { 0x3, "PT_INTERP" }, 
    { 0x4, "PT_NOTE" },
    { 0x5, "PT_SHLIB" },
    { 0x6, "PT_PHDR" },
    { 0x70000000, "PT_LOPROC" },
    { 0x7fffffff, "PT_HIPROC" }
};