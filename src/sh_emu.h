#pragma once

#include <sstream>

struct sh_cpu_general_register_set {
    be_uint32_t r[16];
};

struct sh_cpu_control_register_set {
    be_uint32_t sr = 0b1100110000110011;
    be_uint32_t gbr;
    be_uint32_t vbr;
};

struct sh_cpu_system_register_set {
    be_uint32_t mach;
    be_uint32_t macl;

    be_uint32_t pr;
    be_uint32_t pc;
};

struct sh_cpu {
    sh_cpu_general_register_set general_registers;
    sh_cpu_control_register_set control_registers;
    sh_cpu_system_register_set  system_registers;
};

struct sh_emu_state {
    sh_cpu cpu;
    be_uint16_t *instructions;
    be_uint16_t current_instruction;
    be_uint16_t delay_slot;
    
    std::stringstream log;
};
